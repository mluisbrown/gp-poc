import UIKit
import AVFoundation

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    let videoView: UIView = { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        return view
    }(UIView())

    let notesView: UITextView = { view in
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }(UITextView())

    override func viewDidLoad() {
        super.viewDidLoad()

        addViews()
    }

    override func viewDidAppear(_ animated: Bool) {
        showCameraView()
    }

    func addViews() {
        let screenWidth = self.view.bounds.width

        view.addSubview(videoView)
        view.addSubview(notesView)

        let guide = view.safeAreaLayoutGuide

        NSLayoutConstraint.activate([
            videoView.widthAnchor.constraint(equalToConstant: screenWidth / 2),
            videoView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            videoView.topAnchor.constraint(equalTo: guide.topAnchor),
            videoView.bottomAnchor.constraint(equalTo: guide.bottomAnchor)
            ])

        NSLayoutConstraint.activate([
            notesView.widthAnchor.constraint(equalToConstant: screenWidth / 2),
            notesView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            notesView.topAnchor.constraint(equalTo: guide.topAnchor),
            notesView.bottomAnchor.constraint(equalTo: guide.bottomAnchor)
            ])
    }

    func showCameraView() {
        guard UIImagePickerController.isCameraDeviceAvailable(.front) else {
            return
        }

        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.cameraDevice = .front
        addChildViewController(imagePicker)

        videoView.addSubview(imagePicker.view)
        imagePicker.view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            imagePicker.view.leadingAnchor.constraint(equalTo: videoView.leadingAnchor),
            imagePicker.view.trailingAnchor.constraint(equalTo: videoView.trailingAnchor),
            imagePicker.view.topAnchor.constraint(equalTo: videoView.topAnchor),
            imagePicker.view.bottomAnchor.constraint(equalTo: videoView.bottomAnchor)
            ])

        imagePicker.allowsEditing = false
        imagePicker.showsCameraControls = false
    }
}

